<?php

namespace Bss\Fresher\Controller\Fresher;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Bss\Fresher\Model\ModelRepository;
use Magento\Framework\Registry;
use Magento\Framework\Exception\NoSuchEntityException;

class Detail extends Action
{
    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * @var ModelRepository
     */
    protected ModelRepository $modelRepository;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * Construct
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Registry $coreRegistry
     * @param ModelRepository $modelRepository
     */
    public function __construct(
        Context     $context,
        PageFactory $pageFactory,
        Registry    $coreRegistry,
        ModelRepository $modelRepository
    ) {
        $this->pageFactory = $pageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->modelRepository = $modelRepository;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return ResponseInterface|ResultInterface|Page
     * @throws Exception
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('id');
            $viewFaqs = $this->modelRepository->viewFaqs($id);
            $detailFaqs = $this->modelRepository->detailFaqs($id);
            $getCateNor = $this->modelRepository->getCategoryNor();
            if ($this->_coreRegistry->registry('detail')) {
                $this->_coreRegistry->unregister('detail');
            }
            $this->_coreRegistry->register('detail', [
                'id' => $id,
                'view_faq' => $viewFaqs,
                'detail_catenor' => $getCateNor
            ]);
            return $this->pageFactory->create();
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect('bss/fresher/index');
        }
    }
}
