<?php

namespace Bss\Fresher\Controller\Fresher;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;
use Bss\Fresher\Model\ModelRepository;

class Faqs extends Action
{
    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;
    /**
     * @var ModelRepository
     */
    protected ModelRepository $modelRepository;

    /**
     * Construct
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Registry $coreRegistry
     * @param ModelRepository $modelRepository
     */
    public function __construct(
        Context         $context,
        PageFactory     $pageFactory,
        Registry        $coreRegistry,
        ModelRepository $modelRepository
    ) {
        $this->pageFactory = $pageFactory;
        $this->modelRepository = $modelRepository;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('id');
            $getFaqs = $this->modelRepository->getFaqs($id);
            $getFaqsNormal = $this->modelRepository->getFaqsNormal();
            $getCategory = $this->modelRepository->getCategoryNormal();
            if ($this->_coreRegistry->registry('faqs')) {
                $this->_coreRegistry->unregister('faqs');
            }
            $this->_coreRegistry->register('faqs', [
                'faq' => $getFaqs,
                'category' => $getCategory,
                'id_cate' => $id,
                'faq_nor' => $getFaqsNormal
            ]);
            return $this->pageFactory->create();
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->_redirect('bss/fresher/index');
        }
    }
}
