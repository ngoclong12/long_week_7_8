<?php

namespace Bss\Fresher\Controller\Fresher;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Bss\Fresher\Model\ModelRepository;
use Magento\Framework\Registry;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected PageFactory $pageFactory;
    /**
     * @var ModelRepository
     */
    protected ModelRepository $modelRepository;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * Construct
     *
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param Registry $coreRegistry
     * @param ModelRepository $modelRepository
     */
    public function __construct(
        Context         $context,
        PageFactory     $pageFactory,
        Registry        $coreRegistry,
        ModelRepository $modelRepository
    ) {
        $this->pageFactory = $pageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->modelRepository = $modelRepository;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return ResponseInterface|ResultInterface|Page
     * @throws Exception
     */
    public function execute()
    {
        $search = $this->getRequest()->getParam('search');
        $filterFaqsDB = $this->modelRepository->filterFaqsDB($search);
        $getCateNor = $this->modelRepository->getCategoryNor();
        if ($this->_coreRegistry->registry('cate')) {
            $this->_coreRegistry->unregister('cate');
        }
        $this->_coreRegistry->register('cate', [
            'cate_nor' => $getCateNor,
            'search' => $search,
            'filter_faq' => $filterFaqsDB,
        ]);
        return $this->pageFactory->create();
    }
}
