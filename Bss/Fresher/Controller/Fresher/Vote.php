<?php

namespace Bss\Fresher\Controller\Fresher;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Bss\Fresher\Model\FaqsFactory;

class Vote extends Action
{
    /**
     * @var FaqsFactory
     */
    protected FaqsFactory $faqsFactory;

    /**
     * Construct
     *
     * @param Context $context
     * @param FaqsFactory $faqsFactory
     */
    public function __construct(
        Context     $context,
        FaqsFactory $faqsFactory
    ) {
        $this->faqsFactory = $faqsFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $id = $this->_request->getParam('id');
        $checkVote = $this->_request->getParam('checkvote');
        $getVote = $this->faqsFactory->create()->load($id);
        $countLike = $getVote->getData()['liked'];
        $countDislike = $getVote->getData()['disliked'];
        if ($checkVote == 'like') {
            $countLike++;
            $getVote->addData(['liked' => $countLike]);
        }
        if ($checkVote == 'dislike') {
            $countDislike++;
            $getVote->addData(['disliked' => $countDislike]);
        }
        if ($checkVote == 'relike') {
            $countLike--;
            if ($countLike < 0) {
                $countLike = 0;
            }
            $getVote->addData(['liked' => $countLike]);
        }
        if ($checkVote == 'redislike') {
            $countDislike--;
            if ($countDislike < 0) {
                $countDislike = 0;
            }
            $getVote->addData(['disliked' => $countDislike]);
        }
        $getVote->save();

        return $this->resultRedirectFactory->create()->setPath('bss/fresher/index');
    }
}
