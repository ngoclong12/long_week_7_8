<?php

namespace Bss\Fresher\Controller\adminhtml\Faqs;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class AddNew extends Action
{
    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New FAQs'));
        return $resultPage;
    }
}
