<?php

namespace Bss\Fresher\Controller\adminhtml\Faqs;

use Bss\Fresher\Model\FaqsFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\Context;

class DeleteForm extends Action
{
    /**
     * @var FaqsFactory
     */
    private FaqsFactory $faqsFactory;

    /**
     * Construct
     *
     * @param Context $context
     * @param FaqsFactory $faqsFactory
     */
    public function __construct(
        Context     $context,
        FaqsFactory $faqsFactory
    ) {
        $this->faqsFactory = $faqsFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return string
     * @throws \Exception
     */
    public function execute()
    {
        $id = $this->_request->getParam('id');
        $model = $this->faqsFactory->create()->load($id);
        $model->delete();
        $this->messageManager->addSuccess(__('this item has been deleted'));
        return $this->_redirect('bss/faqs/index');
    }
}
