<?php

namespace Bss\Fresher\Controller\adminhtml\Faqs;

use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Bss\Fresher\Model\ResourceModel\Faqs\CollectionFactory;
use Bss\Fresher\Model\FaqsFactory;

class MassSalable extends Action
{
    /**
     * @var FaqsFactory
     */
    private $faqsFactory;
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var RedirectFactory
     */
    private $resultRedirect;

    /**
     * Construct
     *
     * @param Action\Context $context
     * @param FaqsFactory $faqsFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context    $context,
        FaqsFactory       $faqsFactory,
        Filter            $filter,
        CollectionFactory $collectionFactory,
        RedirectFactory   $redirectFactory
    ) {
        parent::__construct($context);
        $this->faqsFactory = $faqsFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    /**
     * Execute
     *
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $total = 0;
        $err = 0;
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time = date("d-m-Y h:i:s A");
        foreach ($collection->getItems() as $item) {
            $model = $this->faqsFactory->create()->load($item->getData('id'));
            $status = $this->_request->getParam('status');
            if ($this->faqsFactory->create()->load($item->getData('id'))->getData('status') != $status) {
                $model->addData(['status' => $status, 'modified' => $time]);
                $model->save();
                $total++;
            } else {
                $err++;

            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('Change success.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'error.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('bss/faqs/index');
    }
}
