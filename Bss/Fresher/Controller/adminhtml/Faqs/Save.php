<?php

namespace Bss\Fresher\Controller\adminhtml\Faqs;

use Bss\Fresher\Model\FaqsFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\Context;

class Save extends Action
{
    /**
     * @var FaqsFactory
     */
    private FaqsFactory $faqsFactory;

    /**
     * Construct
     *
     * @param Context $context
     * @param FaqsFactory $faqsFactory
     */
    public function __construct(Context $context, FaqsFactory $faqsFactory)
    {
        parent::__construct($context);
        $this->faqsFactory = $faqsFactory;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time = date("d-m-Y h:i:s A");
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['id']) ? $data['id'] : null;
        $createdBy = $this->_auth->getUser()->getData('username');
        $newData = [
            'title' => strtolower($data['title']),
            'status' => $data['status'],
            'viewed' => 0,
            'liked' => 0,
            'disliked' => 0,
            'createdby' => $createdBy,
            'created' => $time,
            'modified' => $time,
            'category_id' => $data['category_id'],
            'answer' => $data['answer'],
        ];

        $fixData = [
            'title' => strtolower($data['title']),
            'status' => $data['status'],
            'category_id' => $data['category_id'],
            'answer' => $data['answer'],
            'modified' => $time,
        ];

        $model = $this->faqsFactory->create();

        if ($id) {
            $model->load($id);
            $model->addData($fixData);
        } else {
            $model->addData($newData);
        }
        try {
            $model->save();
            $this->messageManager->addSuccessMessage(__('You saved the FAQs success.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        if ($this->_request->getParam('back')) {
            return $this->resultRedirectFactory->create()->setPath('*/*/addnew', ['id' => $model->getId()]);
        }
        return $this->resultRedirectFactory->create()->setPath('bss/faqs/index');
    }
}
