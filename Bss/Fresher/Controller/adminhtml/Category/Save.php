<?php

namespace Bss\Fresher\Controller\adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\Context;
use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\FaqsFactory;

class Save extends Action
{
    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;
    /**
     * @var FaqsFactory
     */
    private $faqsFactory;

    /**
     * Construct
     *
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param FaqsFactory $faqsFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        FaqsFactory       $faqsFactory
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->faqsFactory = $faqsFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['id']) ? $data['id'] : null;

        $newData = [
            'title' => strtolower($data['title']),
            'status' => $data['status'],
        ];

        $category = $this->categoryFactory->create();

        if ($id) {
            $category->load($id);
        }
        try {
            if (isset($data['image']) && is_array($data['image'])) {
                $strpos = strpos($data['image'][0]['url'], '/media/');
                $data['image'][0]['url'] = substr($data['image'][0]['url'], $strpos + 6);
                $data['image'][0]['url'] = trim($data['image'][0]['url']);
                $newData['image'] = json_encode($data['image']);
            }
            $category->addData($newData);
            $category->save();
            if ($data['status'] == 0) {
                $getFaqs = $this->faqsFactory->create()->getCollection()
                    ->addFieldToFilter('category_id', $id);
                foreach ($getFaqs->getData() as $key => $value) {
                    $this->faqsFactory->create()->load($value['id'])
                        ->addData(['status' => $data['status']])
                        ->save();
                }
            }
            $this->messageManager->addSuccessMessage(__('You saved the category success.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        if ($this->_request->getParam('back')) {
            return $this->resultRedirectFactory->create()->setPath('*/*/addnew', ['id' => $category->getId()]);
        }
        return $this->resultRedirectFactory->create()->setPath('bss/category/index');
    }
}
