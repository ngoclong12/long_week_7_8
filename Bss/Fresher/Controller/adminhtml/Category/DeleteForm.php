<?php

namespace Bss\Fresher\Controller\adminhtml\Category;

use Bss\Fresher\Model\CategoryFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\Context;

class DeleteForm extends Action
{
    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;

    /**
     * Construct
     *
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(Context $context, CategoryFactory $categoryFactory)
    {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Execute
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $id = $this->_request->getParam('id');
        $model = $this->categoryFactory->create()->load($id);
        $model->delete();
        $this->messageManager->addSuccess(__('this item has been deleted'));
        return $this->_redirect('bss/category/index');
    }
}
