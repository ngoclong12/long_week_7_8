<?php

namespace Bss\Fresher\Controller\adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Bss\Fresher\Model\ResourceModel\Category\CollectionFactory;
use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\FaqsFactory;

class MassSalable extends Action
{
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var FaqsFactory
     */
    private $faqsFactory;
    /**
     * @var Filter
     */
    private $filter;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var RedirectFactory
     */
    private $resultRedirect;

    /**
     * Construct
     *
     * @param Action\Context $context
     * @param CategoryFactory $categoryFactory
     * @param FaqsFactory $faqsFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context    $context,
        CategoryFactory   $categoryFactory,
        FaqsFactory       $faqsFactory,
        Filter            $filter,
        CollectionFactory $collectionFactory,
        RedirectFactory   $redirectFactory
    ) {
        $this->faqsFactory = $faqsFactory;
        $this->categoryFactory = $categoryFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
        parent::__construct($context);
    }

    /**
     * Execute
     *
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $total = 0;
        $err = 0;
        foreach ($collection->getItems() as $item) {
            $model = $this->categoryFactory->create()->load($item->getData('id'));
            $status = $this->_request->getParam('status');
            if ($this->categoryFactory->create()->load($item->getData('id'))->getData('status') != $status) {
                $model->addData(['status' => $status]);
                $model->save();
                if ($status == 0) {
                    $getFaqs = $this->faqsFactory->create()->getCollection()
                        ->addFieldToFilter('category_id', $item->getData('id'));
                    foreach ($getFaqs->getData() as $key => $value) {
                        $this->faqsFactory->create()->load($value['id'])
                            ->addData(['status' => $status])
                            ->save();
                    }
                }
                $total++;
            } else {
                $err++;

            }
        }

        if ($total) {
            $this->messageManager->addSuccessMessage(
                __('Change success.', $total)
            );
        }

        if ($err) {
            $this->messageManager->addErrorMessage(
                __(
                    'error.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('bss/category/index');
    }
}
