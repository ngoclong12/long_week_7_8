define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        let arrayCate = config.catedb;
        let arrayTest = config.testdb;
        let idCate = config.idcate;

        arrayCate.forEach(function print(cate) {
            $('.cate_' + cate.id).click(function () {
                $('.qa_' + cate.id).toggle();
                $('.symbol_right_' + cate.id).show();
                $('.symbol_bottom_' + cate.id).hide();
                $('.cate_ans_' + cate.id).hide();
                $('.right_cate_' + cate.id).toggle();
            })
        });

        arrayTest.forEach(function print(test) {
            $('.title_click_' + test.id).click(function () {
                $('.show_' + test.id).toggle();
                $('.title_right_' + test.id).toggle();
                $('.title_bottom_' + test.id).toggle();
            });
        });

        // Show default
        $('.scroll_' + idCate).show();
        $('.right_cate_' + idCate).show();

        // ScrollTo
        let $container = $('#element'),
            $scrollTo = $('.cate_' + idCate);
        
        $container.scrollTop(
            $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
        );

        // Or animate the scrolling:
        $container.animate({
            scrollTop: $scrollTo.offset().top - $container.offset().top + $container.scrollTop()
        });
    };
});


