define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        let array = config.faqs;

        array.forEach(function print(element)
        {
            $('.title_' + element.id).click(function () {
                $('.ans_' + element.id).toggle();
                $('.add_' + element.id).toggle();
                $('.minus_' + element.id).toggle();
            })
        });
    };
});


