define([
    'jquery'
], function ($) {
    'use strict';

    return function (config) {
        let id_ss = config.id;

        // vote like
        $('.vote_like').click(function () {
            $('.vote_dislike').hide();
            $(this).hide();
            $('.vote_relike').show();
            $.ajax({
                url: '/bss/fresher/vote',
                data: {
                    id: id_ss,
                    checkvote: 'like'
                },
                method: 'post',
            })
        });

        //vote relike
        $('.vote_relike').click(function () {
            $('.vote_dislike').show();
            $(this).hide();
            $('.vote_like').show();
            $.ajax({
                url: '/bss/fresher/vote',
                data: {
                    id: id_ss,
                    checkvote: 'relike'
                },
                method: 'post',
            })
        });

        // vote dislike
        $('.vote_dislike').click(function () {
            $('.vote_redislike').show();
            $(this).hide();
            $('.vote_like').hide();
            $.ajax({
                url: '/bss/fresher/vote',
                data: {
                    id: id_ss,
                    checkvote: 'dislike'
                },
                method: 'post',
            })
        });

        // vote redislike
        $('.vote_redislike').click(function () {
            $('.vote_dislike').show();
            $(this).hide();
            $('.vote_like').show();
            $.ajax({
                url: '/bss/fresher/vote',
                data: {
                    id: id_ss,
                    checkvote: 'redislike'
                },
                method: 'post',
            })
        });


    };
});


