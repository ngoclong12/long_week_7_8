<?php

namespace Bss\Fresher\Block;

use Exception;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\FaqsFactory;
use Bss\Fresher\Model\ResourceModel\Process;
use Magento\Framework\Registry;

class Index extends Template
{
    /**
     * @var ResourceConnection
     */
    protected ResourceConnection $resourceConnection;
    /**
     * @var RequestInterface
     */
    protected RequestInterface $request;
    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $categoryFactory;
    /**
     * @var FaqsFactory
     */
    protected FaqsFactory $faqsFactory;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;
    /**
     * @var Process
     */
    private Process $process;

    /**
     * Construct
     *
     * @param Template\Context $context
     * @param CategoryFactory $categoryFactory
     * @param FaqsFactory $faqsFactory
     * @param RequestInterface $request
     * @param Process $process
     * @param ResourceConnection $resourceConnection
     * @param Registry $_coreRegistry
     */
    public function __construct(
        Template\Context   $context,
        CategoryFactory    $categoryFactory,
        FaqsFactory        $faqsFactory,
        RequestInterface   $request,
        Process            $process,
        ResourceConnection $resourceConnection,
        Registry           $_coreRegistry
    ) {
        $this->faqsFactory = $faqsFactory;
        $this->categoryFactory = $categoryFactory;
        $this->request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->_coreRegistry = $_coreRegistry;
        $this->process = $process;
        parent::__construct($context);
    }

    /**
     * GetCategoryDB
     *
     * @return string|void
     */
    public function getCategoryDB()
    {
        return $this->getRepoModel('cate_nor');
    }

    /**
     * GetCountCategory
     *
     * @param int $id
     * @return AbstractDb|AbstractCollection|null
     */
    public function getCountCategory(int $id)
    {
        return $this->process->getCountCategory($id);
    }

    /**
     * FilterFaqsDB
     *
     * @return string|void
     */
    public function filterFaqsDB()
    {
        return $this->getRepoModel('filter_faq');
    }

    /**
     * @return string|void
     */
    function getParamSearch()
    {
        return $this->getRepoModel('search');
    }

    /**
     * @return void
     */
    public function getRepoModel($value)
    {
        $data = '';
        if ($this->_coreRegistry->registry('cate')) {
            $data = $this->_coreRegistry->registry('cate')[$value];
        }
        return $data;
    }
}
