<?php

namespace Bss\Fresher\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;

class GenericButton
{

    /**
     * @var Context
     */
    protected $context;

    /**
     * Construct
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * GetId
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->context->getRequest()->getParam('id');
    }

    /**
     * GetUrl
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
