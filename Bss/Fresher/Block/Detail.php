<?php

namespace Bss\Fresher\Block;

use Exception;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\FaqsFactory;
use Bss\Fresher\Model\ResourceModel\Process;
use Magento\Framework\Registry;

class Detail extends Template
{
    /**
     * @var ResourceConnection
     */
    protected ResourceConnection $resourceConnection;
    /**
     * @var RequestInterface
     */
    protected RequestInterface $request;
    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $categoryFactory;
    /**
     * @var FaqsFactory
     */
    protected FaqsFactory $faqsFactory;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;
    /**
     * @var Process
     */
    private Process $process;
    /**
     * @var RedirectFactory
     */
    private RedirectFactory $redirectFactory;
    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     * Construct
     *
     * @param Template\Context $context
     * @param CategoryFactory $categoryFactory
     * @param FaqsFactory $faqsFactory
     * @param RequestInterface $request
     * @param ResourceConnection $resourceConnection
     * @param Process $process
     * @param Registry $_coreRegistry
     * @param RedirectFactory $redirectFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Template\Context   $context,
        CategoryFactory    $categoryFactory,
        FaqsFactory        $faqsFactory,
        RequestInterface   $request,
        ResourceConnection $resourceConnection,
        Process            $process,
        Registry           $_coreRegistry,
        RedirectFactory    $redirectFactory,
        ManagerInterface   $messageManager
    )
    {
        $this->faqsFactory = $faqsFactory;
        $this->categoryFactory = $categoryFactory;
        $this->request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->_coreRegistry = $_coreRegistry;
        $this->process = $process;
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * GetCategoryDB
     *
     * @return string|void
     */
    public function getCategoryDB()
    {
        return $this->getDetailModel('detail_catenor');
    }

    /**
     * GetCountCategory
     *
     * @param int $id
     * @return AbstractDb|AbstractCollection|null
     */
    public function getCountCategory(int $id)
    {
        return $this->process->getCountCategory($id);
    }

    /**
     * DetailFaqs
     *
     * @param int|null $category_id
     * @return array|Redirect|null
     */
    public function detailFaqs(int $category_id = null)
    {
//        try {
            $id = $this->getDetailModel('id');
            return $this->process->detailFaqs($category_id, $id);
//        } catch (\Magento\Setup\Exception $e) {
//            $this->messageManager->addErrorMessage($e->getMessage());
//            return $this->redirectFactory->create()->setPath('/bss/fresher/index');
//        }
//        return $result;
    }

    /**
     * ViewFaqs
     *
     * @return string|void
     * @throws Exception
     */
    public function viewFaqs()
    {
        return $this->getDetailModel('view_faq');
    }

    /**
     * @return void
     */
    public function getDetailModel(string $value)
    {
        $data = '';
        if ($this->_coreRegistry->registry('detail')) {
            $data = $this->_coreRegistry->registry('detail')[$value];
        }
        return $data;
    }
}
