<?php

namespace Bss\Fresher\Block;

use Magento\Framework\View\Element\Template;
use Bss\Fresher\Model\FaqsFactory;
use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\ResourceModel\Faqs\CollectionFactory;
use Magento\Framework\Registry;
use Bss\Fresher\Model\ResourceModel\Process;

class Faqs extends Template
{

    /**
     * @var FaqsFactory
     */
    protected FaqsFactory $faqsFactory;
    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $categoryFactory;
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;
    /**
     * @var Process
     */
    private Process $process;

    /**
     * Construct
     *
     * @param Template\Context $context
     * @param FaqsFactory $faqsFactory
     * @param CategoryFactory $categoryFactory
     * @param CollectionFactory $collectionFactory
     * @param Process $process
     * @param Registry $_coreRegistry
     */
    public function __construct(
        Template\Context  $context,
        FaqsFactory       $faqsFactory,
        CategoryFactory   $categoryFactory,
        CollectionFactory $collectionFactory,
        Process           $process,
        Registry          $_coreRegistry
    ) {
        $this->faqsFactory = $faqsFactory;
        $this->categoryFactory = $categoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->_coreRegistry = $_coreRegistry;
        $this->process = $process;
        parent::__construct($context);
    }

    /**
     * TestDB
     *
     * @param int $id
     * @return \Bss\Fresher\Model\ResourceModel\Faqs\Collection|\Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function testDB($id)
    {
        return $this->process->getFaqs($id);
    }

    /**
     * GetDataFaqs
     *
     * @return \Bss\Fresher\Model\Faqs
     */
    public function getDataFaqs()
    {
        return $this->faqsFactory->create();
    }

    /**
     * GetCategoryDB
     *
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|string|void|null
     */
    public function getCategoryDB()
    {
        return $this->getRepoModel('category');
    }

    /**
     * GetIdCategory
     *
     * @return string|void
     */
    public function getIdCategory()
    {
        return $this->getRepoModel('id_cate');
    }

    /**
     * GetRepoModel
     *
     * @param string $value
     * @return void|string
     */
    public function getRepoModel(string $value)
    {
        $data = '';
        if ($this->_coreRegistry->registry('faqs')) {
            $data = $this->_coreRegistry->registry('faqs')[$value];
        }
        return $data;
    }
}
