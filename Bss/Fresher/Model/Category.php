<?php

namespace Bss\Fresher\Model;

use Magento\Framework\Model\AbstractModel;

class Category extends AbstractModel
{
    /**
     * Construct
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Category::class);
    }
}
