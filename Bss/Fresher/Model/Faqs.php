<?php

namespace Bss\Fresher\Model;

use Magento\Framework\Model\AbstractModel;

class Faqs extends AbstractModel
{

    /**
     * Construct
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Bss\Fresher\Model\ResourceModel\Faqs::class);
    }
}
