<?php

namespace Bss\Fresher\Model\ResourceModel\Faqs;

use Bss\Fresher\Model\Faqs;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Construct
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Faqs::class, \Bss\Fresher\Model\ResourceModel\Faqs::class);
    }
}
