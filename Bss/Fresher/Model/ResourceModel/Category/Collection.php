<?php

namespace Bss\Fresher\Model\ResourceModel\Category;

use Bss\Fresher\Model\Category;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Construct
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(Category::class, \Bss\Fresher\Model\ResourceModel\Category::class);
    }
}
