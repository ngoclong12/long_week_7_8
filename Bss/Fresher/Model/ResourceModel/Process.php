<?php

namespace Bss\Fresher\Model\ResourceModel;

use Bss\Fresher\Model\Faqs;
use Bss\Fresher\Model\Category;
use Exception;
use \Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Message\ManagerInterface;

class Process
{
    /**
     * @var Faqs
     */
    private Faqs $faqs;
    /**
     * @var Category
     */
    private Category $category;
    /**
     * @var RedirectFactory
     */
    private RedirectFactory $redirectFactory;
    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     * @param Faqs $faqs
     * @param Category $category
     */
    public function __construct(
        Faqs     $faqs,
        Category $category
    ) {
        $this->faqs = $faqs;
        $this->category = $category;
    }

    /**
     * @param $id
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getFaqs($id)
    {
        return $this->faqs
            ->getCollection()
            ->addFieldToFilter('category_id', $id)
            ->addFieldToFilter('status', 1);
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getFaqsNormal()
    {
        return $this->faqs
            ->getCollection()
            ->addFieldToFilter('status', 1);
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getCategoryNormal()
    {
        return $this->category
            ->getCollection()
            ->addFieldToFilter('status', 1);
    }


    /**
     * @param $id
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getCategory($id)
    {
        return $this->category
            ->getCollection()
            ->addFieldToFilter('category_id', $id)
            ->addFieldToFilter('status', 1);
    }

    /**
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getCategoryNor()
    {
        return $this->category->getCollection()->addFieldToFilter('status', 1);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function viewFaqs($id)
    {
        $getView = $this->faqs->load($id);
        if (count($getView->getData()) > 0) {

            $countView = $getView->getData('viewed');
            ++$countView;
            $getView->addData(['viewed' => $countView]);
            $getView->save();
            return $getView->getData();
        }
        return false;
    }

    /**
     * @return array|null
     */
    public function filterFaqsDB($search = null)
    {
        $filter = $this->faqs
            ->getCollection()
            ->addFieldToFilter('status', 1);
        if ($search) {
            $search = explode(" ", $search);
            $str = '';
            foreach ($search as $key => $value) {
                $str .= '%' . $value . '%';
            }
            $filter->addFieldToFilter('title', ['like' => $str]);
        } else {
            $filter->setOrder('viewed', 'DESC')->setPageSize(5);
        }
        return $filter->getData();
    }

    /**
     * @param int|null $category_id
     * @param $id
     * @return array|null
     */
    public function detailFaqs(int $category_id = null, $id)
    {
        $detail = $this->faqs
            ->getCollection()
            ->addFieldToFilter('status', 1);
        if ($category_id != null) {
            $detail->addFieldToFilter('category_id', $category_id);
        } else {
            $detail->addFieldToFilter('id', $id);
        }
        return $detail->getData();
    }

    /**
     * @param int $id
     * @return \Magento\Framework\Data\Collection\AbstractDb|\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection|null
     */
    public function getCountCategory(int $id)
    {
        return $this->faqs->getCollection()->addFieldToFilter('category_id', $id);
    }
}
