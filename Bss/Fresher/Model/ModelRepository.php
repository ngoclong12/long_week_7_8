<?php

namespace Bss\Fresher\Model;

use Exception;
use Bss\Fresher\Model\ResourceModel\Process;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class ModelRepository
{
    /**
     * @var Process
     */
    private Process $process;

    /**
     * @param Process $process
     */
    public function __construct(
        Process $process
    )
    {
        $this->process = $process;
    }

    /**
     * @return AbstractCollection|AbstractDb
     * @throws NoSuchEntityException
     */
    public function getFaqs($id)
    {
        $result = $this->process->getFaqs($id);
        if (count($result->getData()) < 1) {
            throw new NoSuchEntityException(
                __("The FAQs that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getFaqsNormal()
    {
        $result = $this->process->getFaqsNormal();
        if (count($result->getData()) < 1) {
            throw new NoSuchEntityException(
                __("The FAQs that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     * @throws NoSuchEntityException
     */
    public function getCategoryNormal()
    {
        $result = $this->process->getCategoryNormal();
        if (count($result->getData()) < 1) {
            throw new NoSuchEntityException(
                __("The Category that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @return AbstractDb|AbstractCollection|null
     * @throws NoSuchEntityException
     */
    public function getCategory($id)
    {
        $result = $this->process->getCategory($id);
        if (count($result->getData()) < 1) {
            throw new NoSuchEntityException(
                __("The Category that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getCategoryNor()
    {
        $result = $this->process->getCategoryNor();
        if (count($result->getData()) < 1) {
            throw new NoSuchEntityException(
                __("The Category that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function viewFaqs($id)
    {
        $result = $this->process->viewFaqs($id);
        if ($result == false) {
            throw new NoSuchEntityException(
                __("The FAQs that was requested doesn't exist.")
            );
        }
        return $result;
    }

    /**
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function filterFaqsDB($search = null)
    {
        return $this->process->filterFaqsDB($search);
    }
    
    public function detailFaqs($id){
        $result = $this->process->detailFaqs($category_id=null, $id);
        if ($result == false) {
            throw new NoSuchEntityException(
                __("The FAQs that was requested doesn't exist.")
            );
        }
        return $result;
    }
}
