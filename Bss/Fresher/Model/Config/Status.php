<?php

namespace Bss\Fresher\Model\Config;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * ToOptionArray
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => '1', 'label' => __('Enable')],
            ['value' => '0', 'label' => __('Disable')]
        ];
    }
}
