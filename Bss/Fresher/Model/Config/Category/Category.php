<?php

namespace Bss\Fresher\Model\Config\Category;

use Bss\Fresher\Model\ResourceModel\Category\CollectionFactory;

class Category implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * Construct
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collection = $collectionFactory;
    }

    /**
     * ToOptionArray
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        $items = $this->collection->create()->getItems();
        $result = [];
        foreach ($items as $item) {
            $data = $item->getData();
            $result[] = ['value' => $data['id'], 'label' => $data['title']];
        }
        return $result;
    }
}
