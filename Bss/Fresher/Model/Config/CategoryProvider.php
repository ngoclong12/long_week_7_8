<?php

namespace Bss\Fresher\Model\Config;

use Bss\Fresher\Model\CategoryFactory;
use Bss\Fresher\Model\ResourceModel\Category\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Store\Model\StoreManagerInterface;

class CategoryProvider extends AbstractDataProvider
{
    /**
     * @var $_loadedData;
     */
    protected $_loadedData;
    /**
     * @var $collection;
     */
    protected $collection;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * GetData
     *
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        $url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        foreach ($items as $item) {
            $data = $item->getData();
            if (isset($data['image'])) {
                $imageData = json_decode($data['image'], true);
                $data['image'] = [
                    [
                        'name'        => $imageData[0]['name'],
                        'url'         => $url . $imageData[0]['url'],
                        'previewType' => $imageData[0]['previewType'],
                        'id'          => $imageData[0]['id'],
                        'size'        => $imageData[0]['size']
                    ]
                ];
            }
            $this->_loadedData[$item->getId()] = $data;
        }
        return $this->_loadedData;
    }
}
